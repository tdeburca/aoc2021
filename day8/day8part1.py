#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
import sys
import copy

def main():
    lines = getLines()
    signal = ['acedgfb', 'cdfbe', 'gcdfa', 'fbcad', 'dab', 
                'cefabd', 'cdfgeb', 'eafb', 'cagedb', 'ab']
    #output_value = ['cdfeb', 'fcadb', 'cdfeb', 'cdbaf']

    # dict to look up the string for each number.
    lookup = {}
    for i in range(0,10):
        lookup[i] = set()
    # work out the ones with the unique lengths.
    # 1,7,4,8
    for s in signal:
        if len(s) == 2:
            for q in s:
                lookup[1].add(q)
            signal.remove(s)
        elif len(s) == 3:
            for q in s:
                lookup[7].add(q)
            signal.remove(s)
        elif len(s) == 4:
            for q in s:
                lookup[4].add(q)
            signal.remove(s)
        elif len(s) == 7:
            for q in s:
                lookup[8].add(q)
            signal.remove(s)
    
    # 2,3,5
    for s in signal:
        digit = set()
        for w in s:
            digit.add(w)
        # 2,3,5 have a length of 5
        if len(s) == 5:
            # 7 is in 3
            if lookup[7].issubset(digit):
                lookup[3] = digit
                signal.remove(s)
            else:
                pass
                # print(digit, "is 2 or 5")
    
    # to test remove remaining length 5 signals
    for s in signal:
        if len(s) == 5:
            signal.remove(s)
    
    for s in signal:
        print(s, len(s))
    #0,6,9
    for s in signal:
        digit = set()
        for w in s:
            digit.add(w)
        # 6,9,0 have a length of 6
        # len(s) == 6:
        if len(s) == 6:
            # 7 is in 0
            if lookup[7].issubset(digit):
                lookup[0] = digit
                signal.remove(s)
            # 6 does not contain 7
            # TODO: Is there a better way to say this?
            elif not digit.issubset(lookup[7]):
                lookup[6] = digit
                signal.remove(s)
            else:
                print(digit, "is 0 or 9")

    # print(lookup)


def getLines():
    #get list of lines
    # x1,y1 -> x2,y2
    lines = []
    with open('input1.txt') as f:
    # with open('test1.txt') as f:
        for line in f:
            lines.append(line.strip())
    
    output = []
    for line in lines:
        output.append(line.split(' | '))
    out_clean = []
    for o in output:
        q = []
        q.append(o[0].split())
        q.append(o[1].split())
        out_clean.append(q)
    return out_clean


if __name__ == '__main__':
    main()
