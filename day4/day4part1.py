#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
import sys
import copy

def main():
    #get list of directions
    cards = []
    with open('input1.txt') as f:
    # with open('test1.txt') as f:
        for card in f:
            cards.append(card.strip())

    moves = cards.pop(0)
    # cards, list of lists. card[cardnum][row][entry]
    cards= get_cards(cards)
    # convert comma separated list of strings to numbers.
    moves = list(map(int, moves.split(',')))

    zero_score = [[0, 0, 0, 0, 0], 
                  [0, 0, 0, 0, 0], 
                  [0, 0, 0, 0, 0], 
                  [0, 0, 0, 0, 0], 
                  [0, 0, 0, 0, 0]]

    # list of empty score cards:
    scores = []
    for i in range(0,len(cards)):
        scores.append(zero_score)

    for move in moves:
        for i in range(0,len(cards)):
            # oh sweet jesus deep copy... again.
            # From T:
            # Btw you know you can go, like, list(somelist) to make a copy?
            working_card = copy.deepcopy(cards[i])
            working_score = copy.deepcopy(scores[i])
            working_score = evaluateMove(move, working_card, working_score)
            scores[i] = working_score
            if IWin(working_score):
                print('card:', i+1, 'last move:', move)
                print(working_card)
                print(working_score)
                print(getScore(move, working_card, working_score))
                sys.exit()

def getScore(move, working_card, working_score):
    score = 0
    for row_idx, row in enumerate(working_score):
        for num_idx, num in enumerate(row):
            if num == 0:
                score += working_card[row_idx][num_idx]
    score = score * move
    return score 


def IWin(working_score):
    for row in working_score:
        if sum(row) == 5:
            return 1
    # get columns
    columns = []
    for c in range(0, len(working_score)):
        col = []
        for i in working_score:
            col.append(i[c])
        columns.append(col)
    for column in columns:
        if sum(column) == 5:
            return 1
    return 0 



def evaluateMove(move, working_card, working_score):
    # for idx, val in enumerate(ints):
    for row_idx, row in enumerate(working_card):
        for num_idx, num in enumerate(row):
            if num == move:
                working_score[row_idx][num_idx] = 1
    return working_score

def get_cards(cards):
    # get cards
    num_cards = int(len(cards)/6)
    card_list = []
    for i in range(0,num_cards):
        start = i*6
        x = cards[start:start + 6]
        x.pop(0)
        # list of lists: thing[x][y]
        bingocard = []
        for line in x:
            line = list(map(int, line.split()))
            bingocard.append(line)
        card_list.append(bingocard)
    return card_list


if __name__ == '__main__':
    main()
