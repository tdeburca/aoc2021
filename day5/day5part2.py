#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
import sys
import copy

def main():
    lines = getLines()
    maxX, maxY = getGirdDimensions(lines)
    g = grid(maxX, maxY)
    for line in lines:
        x1, y1, x2, y2 = line2points(line)
        l = getLineFromPoints(x1, y1, x2, y2)
        for point in l:
            g.setPoint(point[0],point[1])
            
    g.prettyPrintMap()
    print(g.getOverlaps())

class grid:
    def __init__(self, maxX, maxY):
        y_array = []
        # Max dimensions +2 required for diagonal, I don't know why.
        for i in range(0, maxY + 2):
            y_array.append(0)
        x_array = []
        for i in range(0, maxX +2):
            x_array.append(copy.deepcopy(y_array))
        self.ocean_map = x_array 

    def getMap(self):
        return self.ocean_map

    def setPoint(self, x, y):
        self.ocean_map[y][x] += 1

    def printMap(self):
        for i in self.ocean_map:
            print(i)

    def prettyPrintMap(self):
        for i in self.ocean_map:
            for j in i:
                if j == 0:
                    print('.', end = '')
                else:
                    print(j,end = '')
            print()

    def getOverlaps(self):
        overlaps = 0
        for i in self.ocean_map:
            for j in i:
                if j > 1:
                    overlaps += 1
        return overlaps

def getLineFromPoints(x1, y1, x2, y2):
    output = []
    # horisontal
    if (y1 == y2):
        beginning = min([x1,x2])
        end = max([x1,x2]) + 1
        for i in range(beginning,end):
            output.append([i, y1])
    # vertical 
    elif (x1 == x2):
        beginning = min([y1,y2])
        end = max([y1,y2]) + 1
        for i in range(beginning,end):
            output.append([x1, i]) 
    else:
        #length of line in Y direction:
        length = abs(y1-y2)
        # always go from lowest x to highest x
        if x1 < x2:
            x_start, y_start = x1, y1
            x_end, y_end = x2, y2
        if x2 < x1:
            x_start, y_start = x2, y2
            x_end, y_end = x1, y1
        # do the right thing based on slope
        if y_end > y_start:
            for i in range(0,length + 1):
                output.append([x_start, y_start])
                x_start += 1
                y_start += 1
        else:
            for i in range(0,length + 1):
                    output.append([x_start, y_start])
                    x_start += 1
                    y_start -= 1
    return output 


def line2points(line):
    x1,y1 = list(map(int,line[0].split(',')))
    x2,y2 = list(map(int,line[1].split(',')))
    return [x1, y1, x2, y2]

def getGirdDimensions(lines):
    maxX = 0
    maxY = 0
    for line in lines:
        x1, y1, x2, y2 = line2points(line)
        if x1 > maxX:
            maxX = x1
        if x2 > maxX:
            maxX = x2
        if y1 > maxY:
            maxY = y1
        if y2 > maxY:
            maxY = y2
    return maxX, maxY

def getLines():
    #get list of lines
    # x1,y1 -> x2,y2
    lines = []
    with open('input1.txt') as f:
    # with open('test1.txt') as f:
        for line in f:
            lines.append(line.strip())
    tmp = []
    for l in lines:
        tmp.append(l.split(" -> "))
    return tmp


if __name__ == '__main__':
    main()
