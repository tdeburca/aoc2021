#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
import sys
import copy

def main():
    fishlist = getFish()
    duration = 80
    # duration = 18
    # fishlist = [3]
    # Each day, a 0 becomes a 6 and adds a new 8 to the end of the list, 
    # while each other number decreases by 1 if it was present at the start of the day.
    shoal = []
    for i in fishlist:
        shoal.append(lungfish(i))

    # initial state:
    state = []
    for f in shoal:
        state.append(f.getAge())
    print("Initial state:", *state)

    for i in range(1, duration+1):
        ages = []
        newfish = []
        newages = []
        for f in shoal:
            s = f.age()
            ages.append(f.getAge())
            if s == 6:
                n = lungfish()
                newfish.append(n)
                newages.append(n.getAge())
        ages = ages + newages
        shoal = shoal + newfish 
        # print("     Day:", i, "--", *ages)
        print("     Day:", i, "--")

    print("No. of Fish:", len(shoal))

    
class lungfish:
    def __init__(self, timer=8):        
        self.timer = timer

    def age(self):
        self.timer -= 1
        if self.timer < 0:
            self.timer=6
        return self.timer

    def getAge(self):
        return self.timer


def getFish():
    fish = ''
    with open('input1.txt') as f:
    # with open('test1.txt') as f:
        for line in f:
            fish = line.strip()

    fish = fish.split(',')
    fish = list(map(int, fish))
    return fish

if __name__ == '__main__':
    main()
