#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

def main():
    #get list of numbers
    numbers = []
    with open('input1.txt') as f:
        for number in f:
            number = int(number)
            numbers.append(number)

    # get initial depth
    depth = numbers.pop(0)
    total = 0
    #if depth increases from current measurement 
    for measure in numbers:
        if measure > depth:
            total += 1
        depth = measure
    print(total)



if __name__ == '__main__':
    main()
