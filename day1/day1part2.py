#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

def main():
    #get list of numbers
    numbers = []
    # with open('test1.txt') as f:
    with open('input1.txt') as f:
        for number in f:
            number = int(number)
            numbers.append(number)

    windowlist = []
    for i in range(0,(len(numbers)-2)):
        # get each window
        window = numbers[i] + numbers[i+1] + numbers[i+2]
        windowlist.append(window)

    # get initial depth
    depth = windowlist.pop(0)
    total = 0
    #if depth increases from current measurement 
    for measure in windowlist:
        if measure > depth:
            total += 1
        depth = measure
    print(total)


        


if __name__ == '__main__':
    main()
