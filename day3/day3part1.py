#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

def main():
    #get list of directions
    readings = []
    with open('input1.txt') as f:
    # with open('test1.txt') as f:
        for reading in f:
            readings.append(reading.strip())

    # create something to store readings.
    l = []
    for i in readings[0]:
        l.append({'0':0, '1':0})

    # count incidence in reading
    for reading in readings:
        print(reading)
        for bit in range(0,len(reading)):
            if reading[bit] == '1':
                l[bit]['1'] += 1
            elif reading[bit] == '0':
                l[bit]['0'] += 1
            else:
                print("error")
                break
        print(l)
    
    gamma = ''
    for bit in l:
        if bit['1'] > bit['0']:
            gamma += '1'
        else:
            gamma += '0'

    #epislon is invert of gamma.
    epsilon = '' 
    for c in gamma:
        if c == '1':
            epsilon += '0'
        else:
            epsilon += '1'

    gamma = int(gamma, 2)
    epsilon = int(epsilon,2)
    print(gamma, epsilon, gamma*epsilon)   


    

if __name__ == '__main__':
    main()
