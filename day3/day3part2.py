#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

def main():
    #get list of directions
    readings = []
    with open('input1.txt') as f:
    # with open('test1.txt') as f:
        for reading in f:
            readings.append(reading.strip())
    print(get_oxegen_generator_rating(readings) * get_co2_scrubber_rating(readings))

def get_oxegen_generator_rating(readings):
    bit_width = len(readings[0])
    for digit_index in range(0, bit_width):
            common_digit = get_common_digit(digit_index, readings)
            readings = get_matching_records(digit_index, common_digit, readings)
            if len(readings) == 1:
                return int(readings[0],2)

def get_co2_scrubber_rating(readings):
    bit_width = len(readings[0])
    for digit_index in range(0, bit_width):
            common_digit = get_uncommon_digit(digit_index, readings)
            readings = get_matching_records(digit_index, common_digit, readings)
            if len(readings) == 1:
                return int(readings[0],2)


def get_matching_records(position, value, l):
    output = []
    for record in l:
        if int(record[position]) == value:
            output.append(record)

    return output

# get the most common digit at position x from list l
def get_common_digit(position, l):
    ones = 0
    zeroes = 0
    for record in l:
        if record[position] == '1':
            ones += 1
        elif record[position] == '0':
            zeroes += 1
        else:
            print(record, position, "error")
            break
    if ones >= zeroes:
        return 1
    else:
        return 0 

def get_uncommon_digit(position, l):
    ones = 0
    zeroes = 0
    for record in l:
        if record[position] == '1':
            ones += 1
        elif record[position] == '0':
            zeroes += 1
        else:
            print(record, position, "error")
            break
    if ones >= zeroes:
        return 0
    else:
        return 1 

    

if __name__ == '__main__':
    main()
