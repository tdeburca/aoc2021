#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

def main():
    #get list of directions
    directions = []
    with open('input1.txt') as f:
    # with open('test1.txt') as f:
        for direction in f:
            directions.append(direction.strip())

    location = {'horizontal':0, 'depth':0, 'aim':0}

    for direction in directions:
        vector, value = direction.split()
        value = int(value)
        # forward X does two things
        if vector == 'forward':
            # It increases your horizontal position by X units.
            location['horizontal'] += value
            # It increases your depth by your aim multiplied by X.
            depth_increment = location['aim'] * value
            location['depth'] += depth_increment
        # down X increases your aim by X units.
        elif vector == 'down':
            location['aim'] += value
        # up X decreases your aim by X units.
        elif vector == 'up':
            location['aim'] -= value
            
    print(location['horizontal'] * location['depth'])
    

if __name__ == '__main__':
    main()
