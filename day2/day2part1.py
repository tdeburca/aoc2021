#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

def main():
    #get list of directions
    directions = []
    with open('input1.txt') as f:
    # with open('test1.txt') as f:
        for direction in f:
            directions.append(direction.strip())

    location = {'horizontal':0, 'depth':0}

    for direction in directions:
        vector, value = direction.split()
        value = int(value)
        if vector == 'forward':
            location['horizontal']+=value
        elif vector == 'down':
            location['depth']+=value
        elif vector == 'up':
            location['depth']-=value
            
    print(location['horizontal'] * location['depth'])
    

if __name__ == '__main__':
    main()
